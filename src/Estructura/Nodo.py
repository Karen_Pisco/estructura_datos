class Nodo:

    def __init__(self,informacion,siguiente,anterior):
        self.informacion=informacion
        self.siguiente=siguiente
        self.anteiro=anterior

    def establerSiguiente(self,siguiente):
        self.siguiente=siguiente

    def establerAnteriro(self,anterior):
        self.anterior=anterior

    def establecerInformacion(self,informacion):
        self.informacion=informacion

    def obtenerSiguiente(self):
        return self.siguiente

    def obtenerAnterior(self):
        return self.anterior

    def obtenerInformacion(self):
        return self.informacion    
