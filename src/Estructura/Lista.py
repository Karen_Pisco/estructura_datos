from Estructura.Nodo import Nodo
class Lista():

    def __init__(self):
        self.nodo = None
        self.inicio=None
        self.posicion=0

    def obtenerInicio(self):
        return self.inicio

    def esVacia(self):
        return self.inicio == None

    def push(self,informacion):
        
        self.nodo =  Nodo(informacion,None,None)

        if(self.esVacia()):
        
            self.inicio = self.nodo 
        
        else:
        
            self.nodo.establerSiguiente(self.inicio)
            self.inicio.establerAnteriro(self.nodo)
            self.inicio =  self.nodo
        
        self.posicion += 1
    
    def listar(self):
        lista = self.inicio
        while(lista!=None):
            print(lista.obtenerInformacion())
            lista = lista.obtenerSiguiente()

    def append(self,informacion):
         
        self.nodo =  Nodo(informacion,None,None)

        if(self.esVacia()):
            self.inicio = self.nodo 
            return
        
        ayudante= self.inicio

        
        while(ayudante.obtenerSiguiente()!=None):
            ayudante = ayudante.obtenerSiguiente()
        
        ayudante.establerSiguiente(self.nodo)
        self.nodo.establerAnteriro(ayudante)
        self.posicion += 1

    def extend(self,informacion):
        pass

    def insert(self,informacion,posicion):
        
        i = 1
        ayudante = self.inicio

        if(posicion >= 1 and posicion <= self.posicion):
            
            while(posicion>=i):
                if(i==posicion):
                    ayudante.establecerInformacion(informacion)
                    return
                ayudante=ayudante.obtenerSiguiente()
                i+=1
            
           

        print("este indice no existe escoja un indice en entre 1 y "+str(self.posicion) )

    def remove(self):
        if(self.esVacia()):
            print("la lista esta vacia :")
        else:
            print("se elimino el siguiente elemento: "+str(self.inicio.obtenerInformacion()))
            self.inicio = self.inicio.obtenerSiguiente()

        self.posicion -=1

    def pop(self,posicion):
        i = 1
        ayudante = self.inicio

        if(posicion >= 1 and posicion <= self.posicion):
            
            while(posicion>=i):
                if(i==posicion):
                    print("se elimino el siguiente elemento "+str(ayudante.obtenerInformacion()))
                    ayudante.obtenerAnterior().establerSiguiente(ayudante.obtenerSiguiente())
                    self.posicion -=1
                    return
                ayudante=ayudante.obtenerSiguiente()
                i+=1
        
    def index(self,):
        if(self.esVacia()):
            print("la lista esta vacia :")
        else:
            print("elemento: "+str(self.inicio.obtenerInformacion()))
            
    def count(self,informacion):
        lista = self.inicio
        contador = 0
        while(lista!=None):
            if(lista.obtenerInformacion()== informacion):
                contador +=1
            lista = lista.obtenerSiguiente()
        if(contador==0):
            print("este elemento: "+str(informacion)+" no existe en la lista")    
            return
        print("el elemento :"+str(informacion)+", aparece "+str(contador)+" veces")

    def sort(self):
        pass

    def reverse(self):
        nuev_lista = Lista()
        lista = self.inicio
        while(lista!=None):
            nuev_lista.push(lista.obtenerInformacion())
            lista = lista.obtenerSiguiente()
        self.inicio = nuev_lista.obtenerInicio()
